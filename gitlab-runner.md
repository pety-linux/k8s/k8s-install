# Install GitLab Runner via HELM Chart

### Install HELM
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
### ADD Gitlab REPO
```
helm repo add gitlab https://charts.gitlab.io
```
### Install Gitlab Runner via HELM Chart

- create namespace

```
kubectl create ns gitlab
```
- create K8S secret
```
kubectl create secret generic -n gitlab gitlab-cert --from-file=gitlab.dual.edu.crt
```
Note: .crt file must be placed in the same folder

- install gitlab-runner
```
helm upgrade --install --namespace gitlab gitlab-runner --set gitlabUrl=https://gitlab.dual.edu/ --set runnerRegistrationToken=GR1348941i1npBmyssLErYc9dpry- --set rbac.create=true --set certsSecretName=gitlab-cert gitlab/gitlab-runner
```

- values can be stored in values.yaml
```
certsSecretName: gitlab-cert
gitlabUrl: https://gitlab.dual.edu/
rbac:
  create: true
runnerRegistrationToken: GR1348941i1npBmyssLErYc9dpry-
```
```
helm upgrade --install --namespace gitlab gitlab-runner -f values.yaml gitlab/gitlab-runner
```
- list
```
helm list -n gitlab
```
- show default chart values
```
helm show values gitlab/gitlab-runner
```
- current release values
```
helm get values gitlab-runner -n gitlab
```
- delete 
```
helm delete  gitlab-runner -n gitlab
```

More: https://docs.gitlab.com/runner/install/kubernetes.html
