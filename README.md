# INSTALLATION STEPS
## 0. Run the commands as "root"

## 1. Configure paswordless SSH & sudo (Optional)
The first thing that we are going to do is to set-up paswordless SSH. First generate the the private/public SSH key-pair. Then distribute public key of master node to remaining worker nodes.
```
ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -N ""
ssh-copy-id nodeX
```
Test it:
```
ssh localhost
```

## 2. Disable SELINUX if enabled (RedHat based distros CentOS, not needed for Amazon Linux 2)
```
sestatus 
setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux
```

## 3. Disable SWAP
```
swapoff -a
vim /etc/fstab  ->  Comment out the swap line
```

## 4. Install & Configure Container Runtime Engine
### Docker
Install Docker:
```
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
yum install -y docker-ce
```

Add your user into docker group (Optional):
```
usermod -aG docker $USER
```

Configure the Docker Cgroup Driver to systemd:
```
sed -i '/^ExecStart/ s/$/ --exec-opt native.cgroupdriver=systemd/' /usr/lib/systemd/system/docker.service 
systemctl daemon-reload
```

Enable and Start Docker
```
systemctl enable docker --now 
systemctl status docker
docker info | grep -i cgroup
```

### Containerd
Install Containerd:
```
yum -y install containerd
```

Generate Default Config:
```
containerd config default > /etc/containerd/config.toml
```

Enable and Start Containerd
```
systemctl enable containerd --now
```

## 5. Enable the br_netfilter module for Cluster communication
a ) net.bridge.bridge-nf-call-iptables
```
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
echo '1' > /proc/sys/net/bridge/bridge-nf-call-ip6tables
echo '1' > /proc/sys/net/ipv4/ip_forward
```
permanent configuration via a separate file:
```
cat <<EOF | tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
net.ipv4.ip_forward = 1
EOF
```
Reload configuration:
```
sysctl --system
```

b) activate br_netfilter & overlay modules
```
modprobe br_netfilter
modprobe overlay

cat <<EOF | tee /etc/modules-load.d/custom.conf
overlay
br_netfilter
EOF
```

## 9. Add the Kubernetes repo
```
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
	     https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF
```

## 9. Install Kubernetes
install latest version:
```
yum install -y kubelet kubeadm kubectl
```
in case of specific version needed:
```
yum install -y kubelet-1.20.0 kubeadm-1.20.0 kubectl-1.20.0

yum install -y yum-plugin-versionlock
yum versionlock kubelet kubeadm kubectl
```

## 10. Enable Kubernetes Service
The kubelet service will not start until you run kubeadm init.
```
systemctl enable kubelet
```

## Create Cluster
## 1. Initialize the cluster using the IP range for Cluster networking.
#### Complete the following section on the MASTER ONLY!
```
kubeadm init --pod-network-cidr=10.244.0.0/16
```
```
--apiserver-advertise-address IP -> can be used if the multiple IPs are available to pick the right one
```
For init command for other Master Node:
```
kubeadm init --control-plane-endpoint="172.31.96.157:6443" --pod-network-cidr=10.244.0.0/16 --upload-cert
```
## 2.Copy the kubeadm join command (save it to a file for later usage)
#### Complete the following section on the MASTER ONLY!:
```
Your Kubernetes control-plane has initialized successfully!

To start using your cluster, you need to run the following as a regular user:

  mkdir -p $HOME/.kube
  sudo cp -i /etc/kubernetes/admin.conf $HOME/.kube/config
  sudo chown $(id -u):$(id -g) $HOME/.kube/config

Alternatively, if you are the root user, you can run:

  export KUBECONFIG=/etc/kubernetes/admin.conf

You should now deploy a pod network to the cluster.
Run "kubectl apply -f [podnetwork].yaml" with one of the options listed at:
  https://kubernetes.io/docs/concepts/cluster-administration/addons/

Then you can join any number of worker nodes by running the following on each as root:

kubeadm join 10.203.0.126:6443 --token bi9pcc.n41pahi1n8hs82fo \
        --discovery-token-ca-cert-hash sha256:5ad5bbc5668939951243155bd39212ac067ceb90fabf2955920d8b7af2d5fb9e
```

## 3. Run join command (from all remaining nodes)
Run the join command that you copied earlier (this command needs to be run as sudo), then check your nodes from the master:
```
kubeadm join 10.203.0.126:6443 --token bi9pcc.n41pahi1n8hs82fo \
        --discovery-token-ca-cert-hash sha256:5ad5bbc5668939951243155bd39212ac067ceb90fabf2955920d8b7af2d5fb9e
```
```
kubectl get nodes
```

## 4. Deploy CNI (from master node)
**Flannel:**
```
kubectl apply -f https://raw.githubusercontent.com/coreos/flannel/master/Documentation/kube-flannel.yml
```
OR 

**Calico:**
```
kubectl apply -f https://docs.projectcalico.org/manifests/calico.yaml
```

Check the cluster state afterwards:
```
kubectl get pods --all-namespaces
```
All Nodes shoud be already in "Ready" state:
```
[root@node1 ~]# kubectl get no
NAME    STATUS   ROLES                  AGE     VERSION
node1   Ready    control-plane,master   8m41s   v1.23.5
node2   Ready    <none>                 6m15s   v1.23.5
node3   Ready    <none>                 2m26s   v1.23.5
```

## 5. Postinstall


- Install HELM
```
curl -fsSL -o get_helm.sh https://raw.githubusercontent.com/helm/helm/main/scripts/get-helm-3
chmod 700 get_helm.sh
./get_helm.sh
```
- [Monitoring](pages/monitoring.md)
- [kubeview](gitlab-runner.md)
- [metallb](metallb.md)
- [GitLab Runner](gitlab-runner.md)
- [Ingress](pages/ingress.md)
- [kubens](pages/kubens.md)
- [kompose](pages/kompose.md)
- [autoscaling](pages/autoscaling.md)
- [k9s](pages/k9s.md)
