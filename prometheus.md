# Install Prometheus
- Prometheus is an open-source systems monitoring and alerting toolkit
- Prometheus collects and stores its metrics as tiem series data

## 1. Add repo
```
helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
helm repo update
```
## 2. Install Prometheus chart
```
helm install prometheus prometheus-community/kube-prometheus-stack
kubectl --namespace default get pods -l "release=prometheus"
```
## 3. Create Service via NodePort
prometheus-grafana-ext.yaml:
```
apiVersion: v1
kind: Service
metadata:
  name: prometheus-grafana-ext
  namespace: default
spec:
  ports:
  - name: http-web
    port: 80
    protocol: TCP
    targetPort: 3000
    nodePort: 30000
  selector:
    app.kubernetes.io/instance: prometheus
    app.kubernetes.io/name: grafana
  type: NodePort
```
```
kubectl create -f prometheus-grafana-ext.yaml
```
## 4. Access the UI
http://NODE-IP:30000/login

username: admin

password: prom-operator

Import Dashboard 6417

More: [here](https://k21academy.com/docker-kubernetes/prometheus-grafana-monitoring/)
