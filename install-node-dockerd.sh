#!/bin/bash
# This Script is used for automated installation of K8S stuff.

## Configure paswordless SSH
ssh-keygen -t rsa -f ~/.ssh/id_rsa -q -N ""
cat ~/.ssh/id_rsa.pub >> ~/.ssh/authorized_keys

## Disable SELINUX
setenforce 0
sed -i --follow-symlinks 's/SELINUX=enforcing/SELINUX=disabled/g' /etc/sysconfig/selinux

## Disable SWAP
swapoff -a
cp /etc/fstab /etc/fstab_orig
sed -e '/swap/ s/^#*/#/' -i /etc/fstab

## Docker
# Configure Docker repositories
yum-config-manager --add-repo https://download.docker.com/linux/centos/docker-ce.repo
# Install Docker
yum install -y docker-ce
# Configure cgroup driver for Docker to systemd
sed -i '/^ExecStart/ s/$/ --exec-opt native.cgroupdriver=systemd/' /usr/lib/systemd/system/docker.service 
systemctl daemon-reload
# Start & Enable Docker Service
systemctl enable docker --now 
systemctl status docker
# Check cgroup driver setting
docker info | grep -i cgroup

## Sysctl parameters
echo '1' > /proc/sys/net/bridge/bridge-nf-call-iptables
echo '1' > /proc/sys/net/bridge/bridge-nf-call-ip6tables
cat <<EOF | tee /etc/sysctl.d/k8s.conf
net.bridge.bridge-nf-call-ip6tables = 1
net.bridge.bridge-nf-call-iptables = 1
EOF

# Enable Module
modprobe br_netfilter
cat <<EOF | tee /etc/modules-load.d/custom.conf
br_netfilter
EOF

## Kubernetes
# Add K8S repository
cat <<EOF > /etc/yum.repos.d/kubernetes.repo
[kubernetes]
name=Kubernetes
baseurl=https://packages.cloud.google.com/yum/repos/kubernetes-el7-x86_64
enabled=1
gpgcheck=0
repo_gpgcheck=0
gpgkey=https://packages.cloud.google.com/yum/doc/yum-key.gpg
	     https://packages.cloud.google.com/yum/doc/rpm-package-key.gpg
EOF

# Install Kubernetes
yum install -y kubelet kubeadm kubectl

# Lock installed versions
yum install -y yum-plugin-versionlock
yum versionlock kubelet kubeadm kubectl

# Enable Kubelet Service
systemctl enable kubelet
