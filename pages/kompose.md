# Install kompose
```
curl -L https://github.com/kubernetes/kompose/releases/download/v1.26.1/kompose-linux-amd64 -o kompose
chmod +x kompose
mv ./kompose /usr/local/bin/
```
Usage:
```
 kompose convert -f docker-compose.yaml
```
