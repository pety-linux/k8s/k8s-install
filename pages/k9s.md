# K9S CLI
K9s is a terminal based UI to interact with your Kubernetes clusters. The aim of this project is to make it easier to navigate, observe and manage your deployed applications in the wild. K9s continually watches Kubernetes for changes and offers subsequent commands to interact with your observed resources.
More can be found [here](https://k9scli.io/).

**1. Installation**
```
wget https://github.com/derailed/k9s/releases/download/v0.26.6/k9s_Linux_x86_64.tar.gz

tar xvzf k9s_Linux_x86_64.tar.gz

cp k9s /usr/local/bin/
```

**2. Run**
```
k9s
```
