# Deploy Ingress Controller (OLD)

**1. Create Namespace**
```
kubectl create ns ingress-space
```

**2. Create ConfigMap**
```
kubectl create cm nginx-configuration -n ingress-space
```

**3. Create ConfigMap**
```
kubectl create sa ingress-serviceaccount -n ingress-space
```

**4. Create Role**
```
apiVersion: rbac.authorization.k8s.io/v1
kind: Role
metadata:
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
  name: ingress-role
  namespace: ingress-space
rules:
- apiGroups:
  - ""
  resources:
  - configmaps
  - pods
  - secrets
  - namespaces
  verbs:
  - get
- apiGroups:
  - ""
  resourceNames:
  - ingress-controller-leader-nginx
  resources:
  - configmaps
  verbs:
  - get
  - update
- apiGroups:
  - ""
  resources:
  - configmaps
  verbs:
  - create
- apiGroups:
  - ""
  resources:
  - endpoints
  verbs:
  - get
```

**5. Create Role Binding**
```
apiVersion: rbac.authorization.k8s.io/v1
kind: RoleBinding
metadata:
  labels:
    app.kubernetes.io/name: ingress-nginx
    app.kubernetes.io/part-of: ingress-nginx
  name: ingress-role-binding
  namespace: ingress-space
roleRef:
  apiGroup: rbac.authorization.k8s.io
  kind: Role
  name: ingress-role
subjects:
- kind: ServiceAccount
  name: ingress-serviceaccount
```


**6. Deploy Ingress Controller**
```
apiVersion: apps/v1
kind: Deployment
metadata:
  name: ingress-controller
  namespace: ingress-space
spec:
  replicas: 1
  selector:
    matchLabels:
      name: nginx-ingress
  template:
    metadata:
      labels:
        name: nginx-ingress
    spec:
      serviceAccountName: ingress-serviceaccount
      containers:
        - name: nginx-ingress-controller
          image: quay.io/kubernetes-ingress-controller/nginx-ingress-controller:0.21.0
          args:
            - /nginx-ingress-controller
            - --configmap=$(POD_NAMESPACE)/nginx-configuration
            - --default-backend-service=ingress-space/default-http-backend
          env:
            - name: POD_NAME
              valueFrom:
                fieldRef:
                  fieldPath: metadata.name
            - name: POD_NAMESPACE
              valueFrom:
                fieldRef:
                  fieldPath: metadata.namespace
          ports:
            - name: http
              containerPort: 80
            - name: https
              containerPort: 443
```
NOTE: it is necessary to create Backed Service first
- deployment 
- expose such deployment

**7. Create Ingress Service**

_Option I._
```
kubectl expose -n ingress-space deployment ingress-controller --type=NodePort --port=80 --name=ingress --dry-run=client -o yaml > ingress.yaml + nodeport + namespace
```

_Option II._
```
apiVersion: v1
kind: Service
metadata:
  name: ingress
  namespace: ingress-space
spec:
  type: NodePort
  ports:
  - port: 80
    targetPort: 80
    protocol: TCP
    nodePort: 30080
    name: http
  - port: 443
    targetPort: 443
    protocol: TCP
    name: https
  selector:
    name: nginx-ingress
```


# Deploy Ingress Controller (NEW)
**1. Deploy Ingress Controller for NodePort**
```
kubectl apply -f https://raw.githubusercontent.com/kubernetes/ingress-nginx/controller-v1.1.1/deploy/static/provider/baremetal/deploy.yaml
```

More: https://platform9.com/learn/v1.0/tutorials/nodeport-ingress


# Deploy Ingress Resource
**1. Deploy APP**
```
kubectl create ns rocky-app

kubectl create deployment webapp-rocky1 --image=petyb/rocky:1 --replicas=3 -n rocky-app

kubectl expose deployment webapp-rocky1 -n rocky-app --name rocky1-service --port 8080 --target-port=80
```

**2. Deploy Ingress Resource**
```
apiVersion: networking.k8s.io/v1
kind: Ingress
metadata:
  name: ingress-rocky-app
  namespace: rocky-app
  annotations:
    nginx.ingress.kubernetes.io/rewrite-target: /
    nginx.ingress.kubernetes.io/ssl-redirect: "false"
spec:
  ingressClassName: nginx
  rules:
  - host: "rocky.opensovereigncloud.com"
    http:
      paths:
      - pathType: Prefix
        path: "/"
        backend:
          service:
            name: rocky1-service
            port:
              number: 8080
```

**3. Access App**

http://rocky.opensovereigncloud.com:30573/
